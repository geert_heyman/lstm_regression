import os

import tensorflow as tf
import matplotlib.pyplot as plt


class PredictionPlotter(tf.train.SessionRunHook):

  def __init__(self, every_steps, step_size, features, labels, predictions, mode, model_dir):

    self._timer = tf.train.SecondOrStepTimer(every_steps=every_steps)
    self._step_size = step_size
    self._features = features
    self._labels = labels
    self._predictions = predictions
    self._mode = mode
    self._model_dir = model_dir

  def plot_filename(self, step):
    filename = "%s.png" % self.plot_name(step)
    return os.path.join(self._model_dir, filename)

  def plot_name(self, step):
    if self._mode == tf.estimator.ModeKeys.TRAIN:
      return "train_%i" % step
    elif self._mode == tf.estimator.ModeKeys.EVAL:
      return "eval_%i" % step
    else:
      return "infer_%i" % step

  def begin(self):

    def _float_range(begin, end):
      return tf.cast(tf.range(begin, end), dtype=tf.float32)

    self._timer.reset()
    self._iter_count = 0
    ys_input = self._features["ys_input"]
    input_len = tf.shape(ys_input)[1]
    xs_input = _float_range(0, input_len) * tf.constant(self._step_size, tf.float32)
    global_step = tf.train.get_global_step()
    if self._mode == tf.estimator.ModeKeys.TRAIN:
      ys_predicted = self._predictions["ys_target"]
      xs_output = _float_range(1, input_len + 1) * tf.constant(self._step_size, tf.float32)
      ys_gt = self._labels["ys_target"]
      signal_length = input_len + 1

    else:
      ys_predicted = self._predictions["ys_future"]
      ys_gt = self._labels["ys_future"]
      output_len = tf.shape(ys_gt)[1]
      xs_output =_float_range(input_len, input_len + output_len) * tf.constant(self._step_size, tf.float32)
      signal_length = input_len + output_len

    self._fetches = (global_step, xs_input, ys_input, xs_output, ys_predicted, ys_gt, signal_length)

  def before_run(self, _run_context):
    self._should_trigger = self._timer.should_trigger_for_step(self._iter_count)
    if self._should_trigger:
      return tf.train.SessionRunArgs(self._fetches)
    else:
      return None

  def after_run(self, _run_context, run_values):
    if self._should_trigger:
      step, xs_input, ys_input, xs_output, ys_predicted, ys_gt, length = run_values.results
      plt.figure(figsize=(10, 20))

      plt.title(self.plot_name(step))
      plt.xlabel("x")
      plt.ylabel("y")

      plt.plot(xs_input, ys_input[0].flatten())
      plt.plot(xs_output, ys_gt[0].flatten())
      plt.plot(xs_output, ys_predicted[0].flatten())

      plt.savefig(self.plot_filename(step))
      plt.close()
      self._timer.update_last_triggered_step(self._iter_count)

    self._iter_count += 1
