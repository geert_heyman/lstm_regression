import matplotlib.pyplot as plt

from utils import load_arrays
import numpy as np

sine_data = load_arrays("sine_squared_quadratic_increase.npz")
ys_train = np.concatenate([sine_data["train"], sine_data["val"],  sine_data["test"]], axis=0)



plt.figure(figsize=(10, 6))


#plt.scatter(xs_input, ys_input, color="g")
#plt.scatter(xs_output, ys_predicted, color="b")
#plt.scatter(xs_output, ys_gt, color="g")

plt.plot(ys_train, "o")

plt.show()