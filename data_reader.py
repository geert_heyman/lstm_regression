"""
Reads a .npz with a given signal that is split in three parts: train, val and test and maps it to a tensorflow
Dataset which returns features and labels dictionaries.
  - features: {"ys_input": ys_input}
  - labels" {
            "signal": signal,
            "ys_target": ys_target, # only supplied in training mode
            "ys_future": ys_future, # only supplied in evaluation mode
            }


"""

import tensorflow as tf
import numpy as np


from utils import load_arrays


class DatasetReader(object):

  def __init__(self, signal_file, num_epochs):
    self._signal_file = signal_file
    self._num_epochs = num_epochs

  def name(self):
    return self._signal_file[:-4]

  def create_dataset(self, mode):
    array_dict = load_arrays(self._signal_file)
    ys_target, ys_future = None, None

    if mode == tf.estimator.ModeKeys.TRAIN:
      train = array_dict["train"]
      train = train.reshape((1, -1, 1))
      ys_input = train[:,:-1, :]
      ys_target = train[:,1:, :]
      signal = train

    elif mode == tf.estimator.ModeKeys.EVAL:
      train = array_dict["train"]
      val = array_dict["val"]
      signal = np.concatenate((train, val))
      ys_input = train.reshape((1, -1, 1))
      signal = signal.reshape((1, -1, 1))
      ys_future = val.reshape((1, -1, 1))

    else:
      train = array_dict["train"]
      val = array_dict["val"]
      test = array_dict["test"]
      signal = np.concatenate((train, val, test))
      ys_input = np.concatenate((train, val))
      ys_input = ys_input.reshape((1, -1, 1))
      signal = signal.reshape((1, -1, 1))

    features = {"ys_input": ys_input.astype(np.float32)}
    labels = {"signal": signal.astype(np.float32)}
    if mode == tf.estimator.ModeKeys.TRAIN:
      labels["ys_target"] = ys_target.astype(np.float32)
    if mode == tf.estimator.ModeKeys.EVAL:
      labels["ys_future"] = ys_future.astype(np.float32)

    dataset = tf.data.Dataset.from_tensors((features, labels))
    if mode == tf.estimator.ModeKeys.TRAIN:
      dataset = dataset.repeat(self._num_epochs)
    return dataset



