"""
Generates samples for a family of functions F and splits the data into training, validation and test sets.
The function family is defined as:
F(x; A1, f1, A2, f2, c1, c2, b) = A1 * cos(2 *pi * f1 * x ) +  A2 * sin(2 * pi * f2 * x) + c1 * x +  c2 * x**2 + b
"""


import tensorflow as tf
import numpy as np

from utils import load_yaml
from utils import save_arrays

flags = tf.app.flags
flags.DEFINE_string("function_config", "sine.yml", "Specification of the function hyper-parameters.")
flags.DEFINE_string("sample_config", "sample_config.yml", "Specification of how the data samples are generated "
                                                          "and divided in train, validation and test sets.")
flags.DEFINE_string("output_file", "sine.npz", "Filename under which train, val and test arrays are saved.")
FLAGS = flags.FLAGS


def main(_):
  function_config = load_yaml(FLAGS.function_config)
  sample_config = load_yaml(FLAGS.sample_config)
  f = create_function(function_config)
  train_samples, val_samples, test_samples = sample(f, sample_config)
  save_arrays(FLAGS.output_file, train=train_samples, val=val_samples, test=test_samples)


def create_function(function_config):
  c = function_config

  def f(X):
    y = np.zeros_like(X)

    if c["A1"] != 0.:
      y += c["A1"] * np.sin(c["f1"] * 2 * np.pi * X)
    if c["A2"] != 0.:
      y += c["A2"] * np.power(np.sin(c["f2"] * 2 * np.pi * X), 2)
    if c["c1"] != 0.:
      y += c["c1"] * X
    if c["c2"] != 0.:
      y += c["c2"] * np.power(X, 2)
    if c["b"] != 0.:
      y += np.ones_like(X) * c["b"]
    return y
  return f


def sample(function, sample_config):
  c = sample_config
  X = np.arange(c["num_samples"], dtype=np.float) * c["step_size"]
  Y = function(X)
  if c["noise_std"] != 0.:
    noise = np.random.normal(0, c["noise_std"], c["num_samples"])
    Y += noise
  val_train_size = int(round(c["num_samples"] * 0.8))
  train_size = int(round(val_train_size * 0.8))
  Y_val_train = Y[:val_train_size]
  Y_train = Y_val_train[:train_size]
  Y_val = Y_val_train[train_size:]
  Y_test = Y[val_train_size:]

  return Y_train, Y_val, Y_test


if __name__ == '__main__':
    tf.app.run()