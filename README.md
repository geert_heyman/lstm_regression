# LSTM regressor
Project that uses an LSTM to predict a function (here modelled as a time signal) given a set samples from the function.
Here follows a brief overview on how to use the code and the results of some experiments.

## Install 
The code is written in tensorflow and python 3. You can install all dependencies with:
```pip install -r requirements.txt```

## Training and evaluating a model
Training a model can be done by running the `run.py` script. Examples:

```bash
python run.py # predicts the sine function specified in sine.yml, using default hyper-parameters of the LSTM regressor specified in default_config.yml
python run.py --signal_file sine_squared_quadratic_increase.npz # predicts a more complex periodic function, again with default model hyper-parameters.
python run.py --signal_file sine_squared_quadratic_increase.npz # predicts a more complex periodic function (see sine_squared.yml), again with default model hyper-parameters.
python run.py --model_hparams large_model.yml
```

A model directory will be created in which intermediate checkpoints of the model parameters are created an evaluated"
The visualize the losses in training and validation set run `tensorboard --logdir ./` To visually inspect model performance
look at the train and eval images in the model directory.

## Trying different hyper-parameters
The code allows to play with the following hyper-parameters:
- the number of layers of the LSTM: `num_layers`
- the number of units in each layer of the LSTM: `hidden_dims`
- the learning rate: `learning_rate`
- the norm to use for gradient clipping: `gradient_clip_norm`
- the maximum difference you expect between timesteps (see the time-signal preprocessing section below): `y_max`
- the minimum difference you expect between timesteps (see the time-signal preprocessing section below): `y_min`
- the prefix for the name of the directory in which checkpoints and results are saved: `model_dir`
- num_simulation_steps: the maximum number of steps you want the model to look into the future: `num_simulation_steps`

These parameters can be specified in a yaml file similar to `default_model.yml` and `large_model.yml`.

## Time-signal preprocessing
We perform to preprocessing steps on the time signal:
- removing trends from the signal by mapping the signal `s` to a new signal `s'` with the following formula:
  `s'(t) = s(t) - s(t-1)` (`s(0)=0`)
- rescaling the time-signal such that the values fall approximately in the range: -1, 1. 
For this step the expected maximal and minimal difference between subsequent timesteps should be known. 
*Note*: I did not end up using rescaling in my experiments as I already got good results without it.

## Naming convention data tensors:
- signal: the complete time signal
- ys_input: signal[0:num_train-1], the part of the signal that is input to the lstm regressor
- ys_target: signal[1:num_train], ys_input shifted by one timestep (only used when in training mode)
- ys_future: the part of the signal should be predicted

## Generating other functions
Use `data_generator.py` to generate samples from the following function family:

```
F(x; A1, f1, A2, f2, c1, c2, b) = A1 * cos(2 *pi * f1 * x ) +  A2 * sin(2 * pi * f2 * x) + c1 * x +  c2 * x**2 + b
```

The hyper-parameters A1, f1, ..., b can be configured with yaml file (see sine.yml, sine_squared.yml, ... for examples).
A file with numpy arrays for training, validation and testing is then created with a command such as:

```bash
python data_generator.py --function_config sine.yml --output_file sine.npz
```

## Results

[sine_eval_begin]: https://drive.google.com/uc?export=view&id=1sb_387mqr9PGY5uVMT0u2Mbb9Euj4aIR ""
[sine_eval_end]: https://drive.google.com/uc?export=view&id=1adz_jpJEPWhEECiDvimiC7dfcdIA6kjI ""
[sine_squared_eval_begin]: https://drive.google.com/uc?export=view&id=1nGOf-cHkYNh6nK-9xC8XqbWiwaUcFmJH ""
[sine_squared_eval_end]: https://drive.google.com/uc?export=view&id=1mSuAHFk0_7aIrfo9w78SxwlYSUhQPWoJ ""
[sine_squared_linear_eval_begin]: https://drive.google.com/uc?export=view&id=11Jc5gkgO7UvIivx4bIUwLCCWrDW6yJPS ""
[sine_squared_linear_eval_end]: https://drive.google.com/uc?export=view&id=1eI2YIsIgHVFgRMnPsFjmB7JdljX_UOOb ""
[sine_squared_quadratic_eval_begin]: https://drive.google.com/uc?export=view&id=1MFZRWA216pGm_dnOK0fyPgzQmtYxMBL2 ""
[sine_squared_quadratic_eval_end]: https://drive.google.com/uc?export=view&id=1GVRYMWpn8htenTZsfJ8UXBR5RUvRs_cJ ""

Below some plots that show the training set (blue), the validation set (orange), the signal predicted by the model from
the training set (green). The results are obtained with default model parameters (2 hidden layers, 50 units per layer).

### f(x) = sin(x)
- Left figure: Results at the begin of training (71 steps)
- Right figure: Results after 1329 steps.


![alt text][sine_eval_begin]
![alt text][sine_eval_end]

### f(x) = sin(x) + sin(x)**2 
- Left figure: Results at the begin of training (72 steps)
- Right figure: Results after 1121 steps.


![alt text][sine_squared_eval_begin]
![alt text][sine_squared_eval_end]

### f(x) = sin(x) + sin(x)**2 + x
- Left figure: Results at the begin of training (69 steps)
- Right figure: Results after 1491 steps.


![alt text][sine_squared_linear_eval_begin]
![alt text][sine_squared_linear_eval_end]


### f(x) = sin(x) + sin(x)**2 + x + x**2
- Left figure: Results at the begin of training (73 steps)
- Right figure: Results after 1050 steps.


![alt text][sine_squared_quadratic_eval_begin]
![alt text][sine_squared_quadratic_eval_end]

