"""
Implements a regression model that predicts a function given its history using an LSTM.
"""

import abc

import tensorflow as tf
import yaml

from hooks import PredictionPlotter


class Configurable(object):

  def __init__(self, hparams):
    self._hparams = self._default_hparams()
    self._hparams.override_from_dict(hparams)
    tf.logging.info(
      "Initializing {} instance with hparams: \n{}".format(type(self).__name__, yaml.dump(self._hparams.values())))

  @abc.abstractmethod
  def _default_hparams(self):
    pass


class LSTMRegressor(Configurable):

  def __init__(self, hparams):
    super().__init__(hparams)

  def _default_hparams(self):
    return tf.contrib.training.HParams(
      num_layers=2,
      hidden_dims=50,
      learning_rate=0.001,
      gradient_clip_norm=5.,
      num_simulation_steps=10,
      y_min=-1., # estimate should be based on training data, biggest decrease
      y_max=1., # estimate should be based on training data, biggest increase
      model_dir="",
      plot_every_steps=100,
      step_size=1.,
    )

  def model_fn(self, features, labels, mode):
    self.preprocess(features, labels)
    ys_input = features["ys_input.processed"]  # batch_size x time_dim x 1
    ys, ys_future = self.forward_pass(ys_input, mode)
    loss, train_op = None, None
    if mode != tf.estimator.ModeKeys.PREDICT:
      loss = tf.losses.mean_squared_error(ys, labels["ys_target.processed"])
      train_op = self._create_train_op(loss)
    if mode == tf.estimator.ModeKeys.EVAL:
      time_dim = tf.shape(labels["ys_future.processed"])[1]
      ys_future = ys_future[:, :time_dim, :]

      loss_future = tf.losses.mean_squared_error(ys_future, labels["ys_future.processed"])
      tf.summary.scalar("loss_future_ys", loss_future)
    predictions = self.create_predictions(ys, ys_future, loss)
    self.postprocess(features, predictions)

    train_hooks = [PredictionPlotter(
      self._hparams.plot_every_steps,
      self._hparams.step_size,
      features, labels,
      predictions,
      mode,
      self._hparams.model_dir
    )]
    eval_hooks = [PredictionPlotter(
      -1, # plot every evaluation step
      self._hparams.step_size,
      features, labels,
      predictions,
      mode,
      self._hparams.model_dir
    )]
    return tf.estimator.EstimatorSpec(mode, predictions, loss, train_op, training_hooks=train_hooks, evaluation_hooks=eval_hooks)

  def forward_pass(self, ys_input, mode):
    cells = [tf.contrib.rnn.LSTMCell(self._hparams.hidden_dims) for _ in range(self._hparams.num_layers)]
    cell = tf.contrib.rnn.MultiRNNCell(cells)

    outputs, state = tf.nn.dynamic_rnn(cell, ys_input, dtype=tf.float32)
    ys = self.output_layer(outputs)

    ys_future = None
    if mode != tf.estimator.ModeKeys.TRAIN:
      # Simulate chain (predict next value from previously predicted value, not from groundtruth).
      last_lstm_output = state[-1].h
      ys_future = [self.output_layer(last_lstm_output)]
      for _ in range(self._hparams.num_simulation_steps - 1):
        y_input = ys_future[-1]
        last_lstm_output, state = cell(y_input, state)
        ys_future.append(self.output_layer(last_lstm_output))
      ys_future = tf.stack(ys_future, axis=1)  # batch_size x time_dim x 1
      tf.logging.info("ys_future shape: {}".format(ys_future.get_shape()))
    return ys, ys_future

  def output_layer(self, lstm_outputs):
    with tf.variable_scope("output_layer", reuse=tf.AUTO_REUSE):
      return tf.layers.dense(lstm_outputs, 1, activation=None)

  def _create_train_op(self, loss):
    optimizer = tf.train.AdamOptimizer(learning_rate=self._hparams.learning_rate)

    def clip_gradients(grads_and_vars):
      gradients, variables = zip(*grads_and_vars)
      clipped_gradients, _ = tf.clip_by_global_norm(gradients, self._hparams.gradient_clip_norm)
      return list(zip(clipped_gradients, variables))

    return tf.contrib.training.create_train_op(
      loss,
      optimizer,
      transform_grads_fn=clip_gradients)

  def create_predictions(self, ys, ys_future, loss):
    predictions = {"ys_target.processed": ys}
    if ys_future is not None:
      predictions["ys_future.processed"] = ys_future
    if loss != None:
      predictions["loss"] = loss
    return predictions

  def preprocess(self, features, labels):
    """
    Updates adds entries to the features and labels dict that preprocess the sequence to:
      - make the signal stationary: calculate the differentials of subsequent time points.
      - normalize the range to be (roughly) between -1 and 1

    :param features:
    :param labels:
    :return:
    """
    # make signal stationary:
    # rescale the signal s.t. the values are within -1 ... 1 range
    # signal: 1 2 3 4 5 6
    # ys_input: 1 2 3
    # ys: 2 3 4
    # ys_future: 4 5 6
    num_input_steps = tf.shape(features["ys_input"])[1]

    signal = labels["signal"]
    signal_preprocessed = self._preprocess_signal(signal)
    features["ys_input.processed"] = signal_preprocessed[:, :num_input_steps]
    labels["ys_target.processed"] = signal_preprocessed[:, 1:num_input_steps+1]
    labels["ys_future.processed"] = signal_preprocessed[:, num_input_steps:]



  def _preprocess_signal(self, signal):
    # make signal stationary (remove trends).
    signal_diff = signal[:, 1:] - signal[:, :-1]
    zeros = tf.zeros(shape=[tf.shape(signal)[0], 1, 1])
    signal_diff = tf.concat([zeros, signal_diff], axis=1)

    # normalize diffs s.t. they are in range -1 .. 1
    y_min = self._hparams.y_min
    y_max = self._hparams.y_max
    signal_diff_norm = scale(signal_diff, y_min, y_max, -1., 1.)

    return signal_diff_norm

  def postprocess(self, features, predictions):
    """
    Inverse of pre-process.
    """
    
    def _rescale(signal):
      y_min = self._hparams.y_min
      y_max = self._hparams.y_max
      return scale(signal, -1. , 1., y_min, y_max)
    
    y_last_input = features["ys_input"][:, -1:, :]
    if "ys_target.processed" in predictions:
      ys_target = _rescale(predictions["ys_target.processed"])
      ys_target += features["ys_input"]
      predictions["ys_target"] = ys_target
      
    if "ys_future.processed" in predictions:
      ys_future = _rescale(predictions["ys_future.processed"])
      # stationary => original
      ys_future = tf.cumsum(tf.concat([y_last_input, ys_future], axis=1), axis=1)[:, 1:]
      predictions["ys_future"] = ys_future

  def create_estimator(self):
    return tf.estimator.Estimator(self.model_fn, self._hparams.model_dir)


def scale(signal, min, max, new_min, new_max):
  return (signal - min)/(max - min) * (new_max - new_min) + new_min