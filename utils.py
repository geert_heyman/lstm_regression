
import yaml
import numpy as np


def load_yaml(filename):
  with open(filename, "r") as f:
    return yaml.load(f)


def save_arrays(filename, **arrays):
  with open(filename, "wb") as f:
    np.savez(f, **arrays)


def load_arrays(filename):
  return np.load(filename)