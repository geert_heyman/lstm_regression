"""
Script for training and evaluating a model that approximates a time signal.
"""
import tensorflow as tf

from data_reader import DatasetReader
from model import LSTMRegressor
from utils import load_yaml

flags = tf.app.flags
flags.DEFINE_string("model_hparams", "default_model.yml", "Specification of the model hyper-parameters. ")
flags.DEFINE_string("signal_file", "sine.npz", "File that contains the time signal to approximate. ")
flags.DEFINE_integer("num_epochs", 3000, "Number of training epochs that should be used for training.")

FLAGS = flags.FLAGS

tf.logging.set_verbosity(tf.logging.INFO)


def main(_):
  ds_reader = DatasetReader(FLAGS.signal_file, FLAGS.num_epochs)

  def train_input_fn():
    return ds_reader.create_dataset(tf.estimator.ModeKeys.TRAIN)

  def eval_input_fn():
    return ds_reader.create_dataset(tf.estimator.ModeKeys.EVAL)

  model_hparams = load_yaml(FLAGS.model_hparams)
  model_hparams["model_dir"] += "_%s" % ds_reader.name()
  model = LSTMRegressor(model_hparams)
  estimator = model.create_estimator()

  tf.estimator.train_and_evaluate(
    estimator,
    tf.estimator.TrainSpec(train_input_fn, max_steps=FLAGS.num_epochs),
    tf.estimator.EvalSpec(eval_input_fn, None, throttle_secs=20))


if __name__ == '__main__':
    tf.app.run()

